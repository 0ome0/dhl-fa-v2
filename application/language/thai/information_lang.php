<?php
$lang['welcome_message'] = 'Type message in German';
$lang['Dashboard'] = 'สรุปช้อมูล';
$lang['Fixassetlist'] = 'รายการทรัพย์สิน';
$lang['Dormitory'] = 'หอพัก';
$lang['Rooms'] = 'ห้องพัก';
$lang['Customer'] = 'ลูกค้า';
$lang['Invoice'] = 'ใบเสร็จ';
$lang['MaCost'] = 'ค่าบำรุงรักษา';
$lang['Report'] = 'รายงาน';
$lang['DormitoryName'] = 'ชื่อหอพัก';
$lang['Branch'] = 'สาขา';
$lang['Address'] = 'ที่อยู่';
$lang['Save'] = 'บันทึก';
$lang['Cancel'] = 'ยกเลิก';
$lang['Edit'] = 'แก้ไข';
$lang['Delete'] = 'ลบ'; 
$lang['Contact'] = 'ติดต่อ';
$lang['Email'] = 'อีเมล์';
$lang['Tel'] = 'เบอร์โทรศัพท์';
$lang['Fax'] = 'เบอร์ Fax';
$lang['DormitoryList'] = 'รายชื่อหอพัก';
$lang['Option'] = 'ตัวเลือก';
$lang['RoomType'] = 'ประเภทห้องพัก';
$lang['RoomItem'] = 'สิ่งอำนวยความสะดวก';
$lang['RoomInDorm'] = 'จัดระเบียบห้องพัก';
$lang['ViewDatail'] = 'รายละเอียด';
$lang['RoomTypeCode'] = 'รหัสประเภทห้องพัก';
$lang['RoomTypeName'] = 'ชื่อประเภทห้องพัก';
$lang['RoomPrice'] = 'ราคาเริ่มต้น';
$lang['Note'] = 'บันทึกช่วยเตือน';
$lang['RoomTypeList'] = 'รายชื่อประเภทห้องพัก';
$lang['RoomItemCode'] = 'รหัสสิ่งอำนวยความสะดวก';
$lang['savesuccess'] = 'บันทึกสำเร็จ';
$lang['error'] = 'error';
$lang['RoomItemName'] = 'ชื่อสิ่งอำนวยความสะดวก';
$lang['ItemPrice'] = 'ราคา'; 
$lang['RoomItemList'] = 'รายการสิ่งอำนวยความสะดวก';
$lang['PleaseSelectDorm'] = 'กรุณาเลือกหอพัก';
$lang['Next'] = 'ขั้นตอนต่อไป';
$lang['RoomNumber'] = 'หมายเลขห้อง';
$lang['RoomFloor'] = 'ชั้น';
$lang['RoomInDormList'] = 'รายการห้องในหอพัก';
$lang['CustomerName'] = 'ชื่อ';
$lang['CustomerSurName'] = 'นามสกุล';
$lang['CustomerIDCard'] = 'บัตรเลขบัตรประชาชน';
$lang['CustomerAddress1'] = 'ที่อยู่ 1';
$lang['CustomerAddress2'] = 'ที่อยู่ 2';
$lang['Picture'] = 'รูป';
$lang['Deposit'] = 'เงินมัดจำ';
$lang['NameSurName'] = 'ชื่อและ นามสกุล';
$lang['Day'] = 'วัน';
$lang['Week'] = 'สัปดาห์';
$lang['Month'] = 'เดือน';
$lang['Year'] = 'ปี';
$lang['Booking'] = 'จองที่พัก';
$lang['CheckInDate'] = 'วันที่เข้าพัก';
$lang['CheckOutDate'] = 'วันที่ออก';
$lang['CustomerType'] = 'ประเภทลูกค้า'; 
$lang['DepositMonth'] = 'มัดจำเดือน';
$lang['OldCustomer'] = 'ลูกค้าเก่า'; 
$lang['NewCustomer'] = 'ลูกค้าใหม่';
$lang['FileInput'] = 'File input';
$lang['PictureIDCard'] = 'รูปบัตรประชาชน';
$lang['Price'] = 'ราคา';
$lang['ContactDate'] = 'วันทำสัญญา';
$lang['ContactPerson'] = 'ผู้ติดต่อฉุกเฉิน';
$lang['Relatetion'] = 'ความสัมพันธ์'; 
$lang['QtyPerson'] = 'จำนวนผู้เข้าพัก';
$lang['OptionNote'] = 'ส่วนจ่ายเพิ่มเติม';
$lang['DisCountNote'] = 'ส่วนลดราคา';
$lang['Insurrance'] = 'ค่าประกันทรัพย์สิน';
$lang['RoomStatus'] = 'สถานะ';
$lang['RoomManage'] = 'บริหารจัดการ';
$lang['RoomAll'] = 'ห้องทั้งหมด';
$lang['RoomFull'] = 'ห้องที่ไม่ว่าง';
$lang['RoomEmpty'] = 'ห้องว่าง';
$lang['Vat'] = 'ภาษี';
$lang['Quatation'] = 'ใบเสนอราคา';
$lang['Invoice'] = 'ใบเสร็จรับเงิน';
$lang['System'] = 'System';
$lang['Company'] = 'ชื่อบริษัท';
$lang['CompanyAddress'] = 'ที่อยู่บริษัท';
$lang['CompanyID'] = 'เลขประจำตัวผู้เสียภาษี 13 หลัก';
$lang['CompanyID2'] = 'เลขประจำตัวผู้เสียภาษี';
$lang['CusCompany'] = 'บริษัทผู้ถูกหักภาษี';
$lang['CusCompanyAddress'] = 'ที่อยู่ บริษัทผู้ถูกหักภาษี';
$lang['CusCompanyID'] = 'เลขประจำตัวผู้เสียภาษี 13 หลัก ผู้ถูกหักภาษี';
$lang['CusCompanyID2'] = 'เลขประจำตัวผู้เสียภาษี ผู้ถูกหักภาษี';
$lang['VatBook'] =  'หนังสือ หัก ณ ที่จ่าย';
$lang['BookNo'] =  'เล่มที่';
$lang['NumNo'] =  'เลขที่';
$lang['CompanyNameEx'] =  'ผู้มีหน้าที่หักภาษีณ ที่จ่าย';
$lang['CompanyNameEx2'] =  'ผู้ถูกหักภาษีณ ที่จ่าย';
$lang['withHoldingTax'] =  'ภาษีที่หักและนำส่งไว้';
$lang['PayPrices'] =  'จำนวนเงินที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['ListOfWHT'] =  'รายการภาษีหัก ณ ที่จ่าย';
$lang['Date'] =  'วันที่';
$lang['DueDate'] =  'Due Date';
$lang['CustomerID'] =  'Customer ID';
$lang['InvoiceRef'] =  'อ้างอิงใบสั่งจ้าง เลขที่';
$lang['DateRef'] =  'ลงวันที่';
$lang['Company'] =  'บริษัท';
$lang['CompanyDetail'] =  'รายละเอียด บริษัท';
$lang['Contact'] =  'ผู้ติดต่อ';
$lang['Address1'] =  'ที่อยู่ บรรทัด1';
$lang['Address2'] =  'ที่อยู่ บรรทัด2';
$lang['Address3'] =  'ที่อยู่ บรรทัด3';
$lang['taxid'] =  'หมายเลขภาษี';
$lang['website'] =  'website';
$lang['Project'] = 'Project';
$lang['CompanyCode'] = 'รหัสลูกค้า';
$lang['ListOfCustomer'] = 'รายชื่อลูกค้า';
$lang['DoYouWantToDelete'] = 'ต้องการจะลบข้อมูลนี้หรือไม่?';
$lang['NoCancel'] = 'No';
$lang['Yes'] = 'Yes';
$lang['Close'] = 'Close';
$lang['Confirmation'] = 'Confirmation';
$lang['Information'] = 'Information';
$lang['Search'] = 'Search';
$lang['ResetSearch'] = 'Reset Search';
$lang['Search'] = 'Search';
$lang['Add'] = 'Add';
$lang['Total'] = 'Total';
$lang['Records'] = 'Records';
$lang['ResultsPerPage'] = 'ResultsPerPage';
$lang['Previous'] = 'Previous';
$lang['Next'] = 'Next';
$lang['ProjectName'] = 'ชื่อโปรเจค';
$lang['budget'] = 'งบประมาณ';
$lang['note'] = 'บันทึกเพิ่มเติม';
$lang['projectyear'] = 'โปรเจค ปี';
$lang['ListOfProject'] = 'รายชื่อ project';
$lang['Require'] = 'ต้องการ';
$lang['Unit'] = 'Unit';
$lang['DESCRIPTION'] = 'DESCRIPTION';
$lang['Quantity'] = 'Qty';
$lang['Price'] = 'Price';
$lang['Amount'] = 'Amount';
$lang['ITEM'] = 'ITEM';
$lang['SubTotal'] = 'Sub Total';
$lang['VAT'] = 'VAT 7%';
$lang['Total'] = 'Total';
$lang['sub_alphabet'] = 'รวมเงินภาษีที่หักนำส่ง (ตัวอักษร) ';
$lang['SignName'] = 'ลงชื่อผู้จ่ายเงิน';
$lang['SignDate'] = 'วัน เดือน ปี ที่ออกหนังสือรับรอง';
$lang['ListOfQuatation'] = 'รายชื่อ ใบเสนอราคา';
$lang['Print'] = 'พิมพ์';
$lang['Payment'] = 'Perment term and condition'; 
$lang['SocialFund'] = 'ประกันสังคม';
$lang['Fund'] = 'กองทุนสำรองเลี้ยงชีพ';
$lang['Income'] = 'รายรับ รายจ่าย';
$lang['revenue'] = 'รายรับ';
$lang['expenditure'] = 'รายจ่าย';
$lang['Item'] = 'รายละเอียด';
$lang['RequireMoreZero'] = 'ต้องมีค่ามากกว่า 0';
$lang['ListOfIncome'] = 'รายการรายรับ รายจ่าย';
$lang['Status'] = 'Status';
$lang['StatusWorking'] = 'กำลังดำเนินการ';
$lang['StatusPending'] = 'ยกเลิก';
$lang['StatusSuccess'] = 'อนุมัติ';
$lang['PleaseSelectStatus'] = 'กรุณาเลือกสถานะ';
$lang['ReportTotal']= 'รายงานแบบรวมทุก Project';
$lang['ReportSeperate']= 'รายงานแบบแยก Project';