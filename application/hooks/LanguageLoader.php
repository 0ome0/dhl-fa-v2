<?php
 
class LanguageLoader
 
{
 
   function initialize() {
 
       $ci =& get_instance();
 
       $ci->load->helper('language');
	   
	   $default_lang = 'english';//'english';//'thai';
 
       $siteLang = $ci->session->userdata('site_lang');
 
       if ($siteLang) {
 
           $ci->lang->load('information',$siteLang);
 
       } else {
		   
           $ci->lang->load('information', $default_lang);
 
       }
 
   }
 
}