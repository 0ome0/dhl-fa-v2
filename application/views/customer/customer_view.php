<script src="<?php echo base_url('asset/customerController.js');?>"></script>
 <div  ng-controller="customerController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('Customer');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Customer');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyCode');?></label>
								<input class="form-control" ng-model="modelSearch.code" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12"> 
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('Company');?></label>
								<input class="form-control" ng-model="modelSearch.name" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('Contact');?></label>
								<input class="form-control" ng-model="modelSearch.contact"  maxlength="80">
								<p class="help-block"></p>
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Customer');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyCode');?></label>
												<input class="form-control" ng-model="CreateModel.code" maxlength="80" >
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12"> 
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Company');?></label>
												<input class="form-control" ng-model="CreateModel.name" maxlength="80" >
												<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Contact');?></label>
												<input class="form-control" ng-model="CreateModel.contact"  maxlength="80">
												<p class="CreateModel_contact require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('taxid');?></label>
												<input class="form-control" ng-model="CreateModel.taxid"  maxlength="80">
												<p class="CreateModel_taxid require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Tel');?></label>
												<input class="form-control" ng-model="CreateModel.tel"  maxlength="50">
												<p class="CreateModel_tel require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Email');?></label>
												<input class="form-control" ng-model="CreateModel.email"  maxlength="80">
												<p class="CreateModel_email require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('website');?></label>
												<input class="form-control" ng-model="CreateModel.website"  maxlength="80">
												<p class="CreateModel_website require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div> 
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-12 col-md-12 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address1');?></label>
												<textarea class="form-control" rows="3" ng-model="CreateModel.address1"  maxlength="80"></textarea>
												
												<p class="CreateModel_address1 require text-danger"><?php echo $this->lang->line('Require');?></p>
												<br/>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-12 col-md-12 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address2');?></label>
												<textarea class="form-control" rows="3" ng-model="CreateModel.address2"  maxlength="80"></textarea>
												
												<p class="CreateModel_address2 require text-danger"><?php echo $this->lang->line('Require');?></p>
												<br/>
											</div> 
										</div>
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-12 col-md-12 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address3');?></label>
												<textarea class="form-control" rows="3" ng-model="CreateModel.address3"  maxlength="80"></textarea>
												<p class="CreateModel_address3 require text-danger"><?php echo $this->lang->line('Require');?></p>
												<br/>
												<br/>
											</div> 
										</div>
										
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfCustomer');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th class="sorting_asc" ng-click="sort($event)"  sort="code"><?php echo $this->lang->line('CompanyCode');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="name"><?php echo $this->lang->line('Company');?></th>
											<th class="sorting" ng-click="sort($event)"  sort="contact"><?php echo $this->lang->line('Contact');?></th> 
											<?php /*<th><?php echo $this->lang->line('Tel');?></th>
											<th><?php echo $this->lang->line('taxid');?></th> */ ?>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.code"></td> 
                                            <td ng-bind="item.name"></td>
                                            <td ng-bind="item.contact"></td>
                                           <?php /* <td ng-bind="item.tel"></td>
                                            <td ng-bind="item.taxid"></td> */ ?>
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>