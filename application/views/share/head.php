<?php 
	$this->load->helper('url');
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url('/theme/image/favicon.ico');?>" />
    <title>DHL Fix Asset</title>
	
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('theme/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url('theme/metisMenu/metisMenu.min.css');?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('theme/dist/css/sb-admin-2.css');?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url('theme/morrisjs/morris.css');?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('theme/font-awesome/css/font-awesome.min.css');?>"  rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!-- Angular strap -->
    <link href="<?php echo base_url('theme/ng/css/angular-strap.css');?>"  rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('theme/ng/css/select.css');?>"  rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('theme/ng/css/selectize.css');?>"  rel="stylesheet" type="text/css">
	
	
	   <!-- jQuery -->
    <script src="<?php echo base_url('theme/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core JavaScript --> 
	<script src="<?php echo base_url('theme/bootstrap/js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript --> 
	<script src="<?php echo base_url('theme/metisMenu/metisMenu.min.js');?>"></script>

    <!-- Morris Charts JavaScript -->
	<script src="<?php echo base_url('theme/raphael/raphael.min.js');?>"></script>
	<script src="<?php echo base_url('theme/morrisjs/morris.min.js');?>"></script> 

    <!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url('theme/dist/js/sb-admin-2.js');?>"></script>
	
	<!-- Angular js -->
	<script src="<?php echo base_url('theme/ng/angular.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/Utility.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/app.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/json-formatter.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/angular-resource.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/ui-bootstrap-tpls-2.5.0.js');?>"></script>	
	<script src="<?php echo base_url('theme/ng/angular-sanitize.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/select.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/ng-file-upload.js');?>"></script>
	
	<script src="<?php echo base_url('theme/ng/angular-strap.js');?>"></script>
	
	<!-- Service js --->
	<script src="<?php echo base_url('theme/ng/baseService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/customerApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/systemApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/projectApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/quatationApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/invoiceApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/vatApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/incomeApiService.js');?>"></script>
	<script src="<?php echo base_url('theme/ng/reportApiService.js');?>"></script>

	<script src="<?php echo base_url('theme/ng/fixassetApiService.js');?>"></script>
	
	
	<script type="text/javascript">
		function get_base_url(url){
			return <?php echo "'".base_url()."'"; ?> + url; 
		}
	</script>
	<style>
		#overlay {
			background: rgba(0,0,0,0.4);
			width: 100%;
			height: 100%;
			min-height: 100%;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 10000;
		}
		
		img.overlay {
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			margin: auto;
			position: absolute;
			max-height:100px;
		}
	</style>
</head>

<body>
	<div id="overlay" style="display:none;">
        <img src="<?php echo base_url('theme/image/loading.gif');?>" class="overlay" />
    </div>
    <div id="wrapper"  ng-app="myApp">