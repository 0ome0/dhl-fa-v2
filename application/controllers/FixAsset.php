<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FixAsset extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar-fa');
		$this->load->view('fixasset/fixasset_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addFixAsset() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('FixAssetModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['code'] =  isset($dataPost['code'])?$dataPost['code']: 0;
			$data['name'] =  isset($dataPost['name'])?$dataPost['name']: "";
			$data['contact'] = isset($dataPost['contact'])?$dataPost['contact']: "";
			$data['address1'] = isset($dataPost['address1'])?$dataPost['address1']: "";
			$data['address2'] =  isset($dataPost['address2'])?$dataPost['address2']: "";
			$data['address3'] = isset($dataPost['address3'])?$dataPost['address3']: "";
			$data['tel'] =  isset($dataPost['tel'])?$dataPost['tel']: "";
			$data['email'] = isset($dataPost['email'])?$dataPost['email']: "";
			$data['taxid'] = isset($dataPost['taxid'])?$dataPost['taxid']: "";
			$data['deleteflag'] = isset($dataPost['deleteflag'])?$dataPost['deleteflag']: "0";
			//$data['website'] =  isset($dataPost['website'])?$dataPost['website']: "";
			
			//print_r($data);
			
			//$data['update_date'] = $dateRecord;
			//$data['update_user'] = $this->session->userdata('user_name'); 
	  		// load model 
    		if ($data['id'] == 0) { 
    			//$data['delete_flag'] = 0;
				//$data['create_date'] = $dateRecord;
				//$data['create_user'] =  $this->session->userdata('user_name'); 
    			$nResult = $this->FixAssetModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->FixAssetModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteFixAsset(){
		try{
			$this->load->model('FixAssetModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');

			// print_r($dataPost);
			
			$bResult = $this->FixAssetModel->deleteFixAsset($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getFixAssetModelList(){
	 
		try{
			$this->load->model('FixAssetModel','',TRUE); 
			   	
			/*$data['m_id'] =  $this->input->post('mab_m_id');
			
			$limit =  $this->input->post('limit');
			$offset =  $this->input->post('offset');
			$order = $this->input->post('order');
			$direction = $this->input->post('direction');
			 
			$result = $this->FixAssetModel->getFixAssetNameAllList($data, $limit , $offset, $order, $direction); */
			
			$result['status'] = true;
			$result['message'] = $this->lang->line("savesuccess");
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getFixAssetModel(){
	 
		try{
			$this->load->model('FixAssetModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			// print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->FixAssetModel->getFixAssetList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->FixAssetModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->FixAssetModel->getFixAssetModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
    }
    
    public function printQrcode(){
        $asset_id = $this->uri->segment(3);

        // print_r($asset_id);

		$this->load->model('FixAssetModel','',TRUE); 
		// $data['item_detail'] =  $this->Item_asset_model->get_item_by_id($id);
		$item = $this->FixAssetModel->get_item_by_id($asset_id);

		$asset_name = $item[0]['asset_no'];
		$serial_no = $item[0]['invent_no'];
		$qrcode_img = $item[0]['qr_code_img'];
		
        // print_r($qrcode_img);
		// $this->load->view("itasset/layout-qr_code.php", $data);

		//  echo(APPPATH .'fpdf/fpdf.php'); 
		require(APPPATH .'fpdf/Mypdf.php'); 
		
		  $pdf = new FPDF('p','mm', array(48,24));
		  $pdf = new Mypdf('p','mm', array(28,24));
		  $pdf->SetAutoPageBreak(false);
		  $pdf-> AddPage('L');
          
          $url = base_url('assets/images/qrcode/').$qrcode_img;
          $url_string = "'".$url."'";

        //   print_r($url_string);
        // Insert a dynamic image from a URL
        //   $pdf->Image('//localhost/dhl-fa-v2/assets/images/qrcode/5B50BEDAD1C57.png',2,3,18,0,'PNG');
        //   $pdf->Image($url_string,2,3,18,0,'PNG'); 
		  $pdf->Image(base_url().'assets/images/qrcode/'.$qrcode_img,2,3,18,0,'PNG');

		//   $pdf->SetXY(2, -5);
		//   $pdf->RotatedText(5,250,"Example of rotated text",90);

		  $pdf->SetFont('Arial','',5);
	      $pdf->SetXY(21,21);
		  $pdf->Rotate(90); 
		  $pdf->Cell(0,0,"AST:$asset_name",0,0,'L'); 

		  ob_start(); 
		  $pdf -> output ($_SERVER["DOCUMENT_ROOT"].'/assets/images/qrcode_pdf'. 'your_file_pdf.pdf','I');
		  ob_end_flush(); 

    }
	
	public function getFixAssetComboList(){
	 
		try{ 
			$this->load->model('FixAssetModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->FixAssetModel->getFixAssetComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
