<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('income/income_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addIncome() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('IncomeModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['pro_id'] =  isset($dataPost['pro_id'])?$dataPost['pro_id']: "";
			$data['pro_name'] = isset($dataPost['pro_name'])?$dataPost['pro_name']: "";
			$data['input_date'] = isset($dataPost['input_date'])?$dataPost['input_date']: "";
			$data['item'] =  isset($dataPost['item'])?$dataPost['item']: "";
			$data['price'] =  isset($dataPost['price'])?str_replace("," , "" , $dataPost['price']): "";
			$data['type'] =  isset($dataPost['type'])?$dataPost['type']: "";
			$data['note'] =  isset($dataPost['note'])?$dataPost['note']: "";
			//$data['deleteflag'] = isset($dataPost['deleteflag'])?$dataPost['deleteflag']: "0"; 
			
 
	  		// load model 
    		if ($data['id'] == 0) { 
    			//$data['delete_flag'] = 0;
				//$data['create_date'] = $dateRecord;
				//$data['create_user'] =  $this->session->userdata('user_name'); 
    			$nResult = $this->IncomeModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->IncomeModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteIncome(){
		try{
			$this->load->model('IncomeModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->IncomeModel->deleteIncomename($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getIncomeModelList(){
	 
		try{
			$this->load->model('IncomeModel','',TRUE); 
			   	
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->IncomeModel->getIncomeNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->IncomeModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getIncomeModel(){
	 
		try{
			$this->load->model('IncomeModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->IncomeModel->getIncomeNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->IncomeModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->IncomeModel->getIncomeModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getIncomeComboList(){
	 
		try{ 
			$this->load->model('IncomeModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->IncomeModel->getIncomeComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
