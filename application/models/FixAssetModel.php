<?php
  
class FixAssetModel extends CI_Model {
	
    private $tbl_name = 't_fa';
	private $id = 'asset_id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getFixAssetById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getCustomerNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Customer_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getCustomerModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('deleteflag', 0);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['asset_no']) && $dataModel['asset_no'] != ""){
		 	$sql .= " and asset_no like '%".$this->db->escape_str( $dataModel['asset_no'])."%' ";
		}
		
		if(isset($dataModel['asset_description']) && $dataModel['asset_description'] != ""){
		 	$sql .= " and asset_description like '%".$this->db->escape_str( $dataModel['asset_description'])."%' ";
		}
		
		if(isset($dataModel['costcenter']) && $dataModel['costcenter'] != ""){
		 	$sql .= " and costcenter like '%".$this->db->escape_str( $dataModel['costcenter'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name ." WHERE 1=1 ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getFixAssetList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name ." WHERE 1=1 "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		if($order != ""){
			$this->db->order_by($order, $direction);
		}else{
			$this->db->order_by($this->id ,$direction); 
		}
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteFixAsset($id){
		$result = false;
		try{
			// $query = $this->getFixAssetById($id);
			// $modelData;			
			// foreach ($query->result() as $row)
			// {
			   		
			// 	$modelData = array( 
			// 		//'update_date' => date("Y-m-d H:i:s"),
			// 		//'update_user' => $this->session->userdata('user_name'),
			// 		// 'deleteflag' => 1 //$row->Customer_delete_flag 
			// 	); 
			// }
			
			// $this->db->where($this->asseid, $id);
        	// return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			$sql = "Delete FROM ". $this->tbl_name . " WHERE ".$this->id ."=".$id ; 
			return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	
	// public function deleteCustomername($id){
	// 	$result = false;
	// 	try{
	// 		$query = $this->getCustomerNameById($id);
	// 		$modelData;			
	// 		foreach ($query->result() as $row)
	// 		{
			   		
	// 			$modelData = array( 
	// 				//'update_date' => date("Y-m-d H:i:s"),
	// 				//'update_user' => $this->session->userdata('user_name'),
	// 				'deleteflag' => 1 //$row->Customer_delete_flag 
	// 			); 
	// 		}
			
	// 		$this->db->where($this->id, $id);
    //     	return $this->db->update($this->tbl_name, $modelData);
	// 		//return $this->update($id, $modelData);
	// 		//$sql = "Delete FROM ". $this->tbl_name; 
	// 		//return  $this->db->query($sql);
			
	// 	}catch(Exception $ex){
	// 		return $result;
	// 	}
    // }
	
	public function getCustomerComboList(){
		
		$sql = "SELECT id, 	name, contact, address1, address2, address3, tel, email, taxid  FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
    }
    
    //function from dhl-fa-v1
    public function get_item_by_id($asset_id){
        $query = $this->db->get_where('t_fa', array('asset_id' => $asset_id));
        return $result = $query->result_array();
        
    }
	
}
?>