<?php
  
class InvoiceModel extends CI_Model {
	
    private $tbl_name = 'invoice'; 
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getInvoiceById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
 
	public function getInvoiceModel($id){ 
		$this->db->where('id', $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['IssueOrder']) && $dataModel['IssueOrder'] != ""){
		 	$sql .= " and inv.IssueOrder like '%".$this->db->escape_str( $dataModel['IssueOrder'])."%' ";
		}
		
		if(isset($dataModel['cus_id']) && $dataModel['cus_id'] != 0){
		 	$sql .= " and inv.cus_id = ". $dataModel['cus_id']." ";
		}
		
		if(isset($dataModel['pro_id']) && $dataModel['pro_id'] != 0){
		 	$sql .= " and inv.pro_id = ". $dataModel['pro_id']." ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT inv.id FROM ". $this->tbl_name  ." inv LEFT JOIN project pj on inv.pro_id = pj.id WHERE inv.deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getInvoiceModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT inv.id, inv.IssueDate, inv.IssueOrder, inv.pro_id, inv.cus_id, inv.cus_name, inv.due_date, inv.cus_contact, inv.cus_tel, inv.cus_address, inv.com_contact, inv.com_email, inv.com_tel, inv.com_address, inv.sub_total, inv.vat, inv.total, inv.sub_alphabet, inv.sign_name, inv.sign_date, inv.payment, pj.name FROM ". $this->tbl_name . "  inv LEFT JOIN project pj on inv.pro_id = pj.id WHERE inv.deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT ".$offset.", ".$limit;
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteInvoice($id){
		$result = false;
		try{
			$query = $this->getInvoiceById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{ 
				$modelData = array(  
					'deleteflag' => 1  
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData); 
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getInvoiceComboList(){
		
		$sql = "SELECT id, 	IssueOrder, IssueDate, cus_name FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
}
?>