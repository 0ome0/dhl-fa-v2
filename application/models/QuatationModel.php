<?php
  
class QuatationModel extends CI_Model {
	
    private $tbl_name = 'quatation'; 
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getQuatationById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
 
	public function getQuatationModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('id', $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['IssueOrder']) && $dataModel['IssueOrder'] != ""){
		 	$sql .= " and qt.IssueOrder like '%".$this->db->escape_str( $dataModel['IssueOrder'])."%' ";
		}
		
		if(isset($dataModel['cus_id']) && $dataModel['cus_id'] != 0){
		 	$sql .= " and qt.cus_id = ". $dataModel['cus_id']." ";
		}
		
		if(isset($dataModel['pro_id']) && $dataModel['pro_id'] != 0){
		 	$sql .= " and qt.pro_id = ". $dataModel['pro_id']." ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT qt.id FROM ". $this->tbl_name  ." qt LEFT JOIN project pj on qt.pro_id = pj.id WHERE qt.deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getQuatationModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT qt.id, qt.IssueDate, qt.IssueOrder, qt.pro_id, qt.cus_id, qt.cus_name, qt.due_date, qt.cus_contact, qt.cus_tel, qt.cus_address, qt.com_contact, qt.com_email, qt.com_tel, qt.com_address, qt.sub_total, qt.vat, qt.total, qt.sub_alphabet, qt.sign_name, qt.sign_date, qt.payment, qt.status, pj.name FROM ". $this->tbl_name . "  qt LEFT JOIN project pj on qt.pro_id = pj.id WHERE qt.deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT ".$offset.", ".$limit;
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteQuatation($id){
		$result = false;
		try{
			$query = $this->getQuatationById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{ 
				$modelData = array(  
					'deleteflag' => 1  
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData); 
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function updateStatusQuatation($id, $status){
		$result = false;
		try{
			$query = $this->getQuatationById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{ 
				$modelData = array(  
					'status' => $status 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData); 
		}catch(Exception $ex){
			return $result;
		}
    }
	 
	public function getQuatationComboList(){
		
		$sql = "SELECT id, 	IssueOrder, IssueDate, cus_name FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
}
?>