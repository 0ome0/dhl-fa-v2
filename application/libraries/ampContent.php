<?php
/**
 * Amp Content is the simplest WordPress plugin for beginner.
 * Take this as a base plugin and modify as per your need.
 *
 * @package Amp Content
 * @author Amp
 * @license GPL-2.0+
 * @link https://Amp.com/tag/wordpress-beginner/
 * @copyright 2017 Amp, LLC. All rights reserved.
 *
 *            @wordpress-plugin
 *            Plugin Name: Amp Content
 *            Plugin URI: https://Amp.com/tag/wordpress-beginner/
 *            Description: Amp Content is the simplest WordPress plugin for beginner. Take this as a base plugin and modify as per your need.
 *            Version: 3.0
 *            Author: Amp
 *            Author URI: https://Amp.com/
 *            Text Domain: Amp-hello-world
 *            Contributors: Amp
 *            License: GPL-2.0+
 *            License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */
 
/**
 * Adding Submenu under Settings Tab
 *
 * @since 1.0
 */
function Amp_add_menu() {
	add_submenu_page ( "options-general.php", "Amp Plugin", "Amp Plugin", "manage_options", "Amp-hello-world", "Amp_hello_world_page" );
}
add_action ( "admin_menu", "Amp_add_menu" );
 
/**
 * Setting Page Options
 * - add setting page
 * - save setting page
 *
 * @since 1.0
 */
function Amp_hello_world_page() {
	?>
<div class="wrap">
	<h1>
		 Try to Move the content to ampContent custom field...
	</h1>
 
	<form method="post" action="options.php">
            <?php
				settings_fields ( "Amp_hello_world_config" );
				do_settings_sections ( "Amp-hello-world" );
				submit_button ();
			?>
	</form>
</div>
 
<?php
}
 
/**
 * Init setting section, Init setting field and register settings page
 *
 * @since 1.0
 */
function Amp_hello_world_settings() {
	add_settings_section ( "Amp_hello_world_config", "", null, "Amp-hello-world" );
	add_settings_field ( "Amp-hello-world-text", "This is sample Textbox", "Amp_hello_world_options", "Amp-hello-world", "Amp_hello_world_config" );
	register_setting ( "Amp_hello_world_config", "Amp-hello-world-text" );
}
add_action ( "admin_init", "Amp_hello_world_settings" );
 
/**
 * Add simple textfield value to setting page
 *
 * @since 1.0
 */
function Amp_hello_world_options() {
	
	$listPost = get_posts( array(
							'post_type'      => 'review',// 'news',//'testimonial',//'article',//'service',//'promotion', //'operation',//'center_group',//clinic_center,
							'posts_per_page' => -1,
							//'post_status'    => 'any',
							//'post_parent'    => $post->ID
						));
	
	 if ( $listPost ) {
        foreach ( $listPost as $post ) :  
			//print_r( $post); 
			
			 $ampcontent = preg_replace('#(style=("|\')(.*?)("|\'))#', '', $post->post_content); update_field( "amp_content", $ampcontent, $post->ID );
			
			
            ?>
            <li>
			
                <?php echo $post->post_title; ?>  
            </li>
        <?php
        endforeach;  
    }
	
	//print_r($listPost);
	
	?>
<div class="postbox" style="width: 65%; padding: 30px;">
	<input type="text" name="Amp-hello-world-text"
		value="<?php
	echo stripslashes_deep ( esc_attr ( get_option ( 'Amp-hello-world-text' ) ) );
	?>" /> Provide any text value here for testing<br />
</div>
<?php
}
 
/**
 * Append saved textfield value to each post
 *
 * @since 1.0
 */
/*add_filter ( 'the_content', 'Amp_com_content' );
function Amp_com_content($content) {
	return $content . stripslashes_deep ( esc_attr ( get_option ( 'Amp-hello-world-text' ) ) );
}*/