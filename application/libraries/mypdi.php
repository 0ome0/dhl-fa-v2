<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require('fpdf.php');

define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
require(APPPATH .'fpdf/fpdf.php');
require(APPPATH .'fpdi/fpdi.php');

class  mypdi extends FPDI
{
	function __construct($orientation='P', $unit='mm', $size='A4')
	{
		parent::__construct($orientation,$unit,$size);
	}
}
?>