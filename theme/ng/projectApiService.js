'use strict';
myApp.factory('projectApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Project/");
    return {
          
        //Project 
	    listProject: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getProjectModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getProject: function (model, onComplete) {
            baseService.postObject(servicebase + 'getProjectModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveProject: function (model, onComplete) {
            baseService.postObject(servicebase + 'addProject', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteProject: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteProject', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getProjectComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);