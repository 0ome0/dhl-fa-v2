myApp.controller('reportController', ['$scope', '$filter',  'baseService', 'reportApiService', 'projectApiService',  function ($scope, $filter,  baseService, reportApiService, projectApiService ) {
    $scope.CreateModel = {};
	$scope.modelSearch = {};
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('input_date');
    $scope.SortOrder = baseService.setSortOrder('desc');
	
	$scope.listProject = [];
	$scope.TempProjectIndex = {};
	$scope.TempSearchProjectIndex = {};
	$scope.total = 0;
	$scope.netprice = 0;
	$scope.precent = 0;
	
    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

     $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function () {
        $scope.resetModel (); 
		 
		 
		$(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) {  
        $scope.AddNewDevice(); 
		$scope.loadEditData(item);              
    }
	
    $scope.loadEditData = function (item) { 
		$scope.CreateModel.id =  item.id;
		$scope.CreateModel.input_date =  item.input_date;
		$scope.CreateModel.item =  item.item;
		$scope.CreateModel.price =  parseFloat(item.price);
		$scope.CreateModel.type =  item.type;
		
		if($scope.listProject.length > 0){ 
			$scope.listProject.forEach(function (entry, index) {
				if (item.pro_id === entry.id)
					$scope.TempProjectIndex.selected = entry;
			});
		}
	}
	
	$scope.resetModel = function () {

        $scope.CreateModel = { id:0,  item : "", type: "1", price : 0, input_date: new Date() };
		 
		 if($scope.listProject.length > 0){
			$scope.TempProjectIndex.selected =  $scope.listProject[0]; 
		 }
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"name": "",
							"pro_id": 0
						};
						
		$scope.TempSearchProjectIndex = {};
		//$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
	$scope.onInit = function () {   
		
		//$scope.resetModel();
		$scope.resetSearch();
		
		projectApiService.getComboBox(null, function(results){
			
			var result = results.data;
			
			if(true == result.status){
				 $scope.listProject =  result.message
				 
				 if($scope.listProject.length > 0){
					$scope.TempProjectIndex.selected =  $scope.listProject[0]; 
				 }
				 
			}else{
				baseService.showMessage(result.message);
			}
		});
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
        });
		
		//$scope.reload();
    }
	
	$scope.reload = function (){
		 
		if("undefined" == typeof $scope.TempSearchProjectIndex.selected){
			$scope.modelSearch.pro_id = 0;
		}else{
			$scope.modelSearch.pro_id = $scope.TempSearchProjectIndex.selected.id;
		}
		
		reportApiService.listreport($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				 
				$scope.modelDeviceList = result.message; 
				$scope.total = 0;
				$scope.netprice = 0;
				$scope.precent = 0;
				
				$scope.modelDeviceList.forEach(function (entry, index) {
					var dtPrice = parseFloat(entry.price);
								 
					if ( entry.type == 1){ 
						$scope.netprice += dtPrice;		
						$scope.total += dtPrice; 
					} else{
						$scope.netprice -= dtPrice;		
					}
					 
					//entry.price = $scope.addCommas(dtPrice.toFixed(2) );
				});
				
				$scope.netprice = $scope.addCommas($scope.netprice.toFixed(2));
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		
		
		reportApiService.deletereport({id:item.id}, function (result) {  
                 if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

	}
	
	$scope.addCommas = function (nStr) {
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '.00';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
	
	$scope.showPrice = function(item){
		var price = 0.00;
		
		if(item.type == 2){
			price =  (item.price * -1);
		}else{
			price =  item.price;
		} 
		
		return $scope.addCommas(price );
	}
	
	$scope.showColor = function(item){
		 
		if(item.type == 2){
			return "text-danger";
		}else{
			return  "text-primary";
		} 
		 
	}
	
	$scope.showColorEx = function(){
		 
		if($scope.netprice < 0){
			return "text-danger";
		}else{
			return  "text-primary";
		} 
		 
	}
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		
		  
		if($scope.CreateModel.item == ""){
			$(".CreateModel_item").show();
			bResult = false;
		} 
	 
		if($scope.CreateModel.price <= 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
		
		$scope.CreateModel.pro_id = $scope.TempProjectIndex.selected.id;
		$scope.CreateModel.pro_name = $scope.TempProjectIndex.selected.name;
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
       var bValid = $scope.validatecheck();
	  
		if(true == bValid){
            reportApiService.savereport($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });
		}
    }


}]); 