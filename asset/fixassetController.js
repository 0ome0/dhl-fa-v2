myApp.controller('fixassetController', ['$scope', '$filter',  'baseService', 'fixassetApiService', function ($scope, $filter,  baseService, fixassetApiService ) {
     
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {}; 
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('asset_no');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () {  $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function () {
         $scope.resetModel (); 
		 $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice();   
		$scope.loadEditData(item);
             
    }
	
    $scope.loadEditData = function (item) {
        $scope.CreateModel = angular.copy(item); 
	}
	
	$scope.resetModel = function () {

        // $scope.CreateModel = { id:0, code:"", name : "", contact: "", address1 : "", address2: "", address3:"", tel:"", email:"", taxid: "", website: ""};
        $scope.CreateModel = { id:0, asset_no:"", asset_description : "", sno: "", invent_no : "", costcenter: "", dep_key:"", asset_class:"", cap_date:"", accquis_val: "", accum_dep: "",book_val: "",currency: ""};
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							// "code": "",
							// "name": "",
                            // "contact": "",
                            "asset_no": "",
							"asset_description": "",
							"costcenter": "",
						}; 
		$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
        });
		
		//$scope.reload();
    }
	
	$scope.reload = function (){

        // console.log('run reload');
		fixassetApiService.listFixasset($scope.modelSearch, function (results) {  
		var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;  
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
                $scope.modelDeviceList = result.message; 
               
			}else{
				
			}
		})
	}
 
    $scope.onDeleteTagClick = function(item){
		console.log('del');
		fixassetApiService.deleteFixAsset({id:item.asset_id}, function (result) {  
                if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });

    }
   
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.name == ""){
			$(".CreateModel_name").show();
			bResult = false;
		}
		
		if($scope.CreateModel.code == ""){
			$(".CreateModel_code").show();
			bResult = false;
		}
		
		if($scope.CreateModel.contact == ""){
			$(".CreateModel_contact").show();
			bResult = false;
		}
		
		if($scope.CreateModel.taxid == ""){
			$(".CreateModel_taxid").show();
			bResult = false;
		}
		if($scope.CreateModel.tel == ""){
			$(".CreateModel_tel").show();
			bResult = false;
		}
		if($scope.CreateModel.email == ""){
			$(".CreateModel_email").show();
			bResult = false;
		}
		if($scope.CreateModel.website == ""){
			$(".CreateModel_website").show();
			bResult = false;
		}
		
		if($scope.CreateModel.address1 == ""){
			$(".CreateModel_address1").show();
			bResult = false;
		}
		if($scope.CreateModel.address2 == ""){
			$(".CreateModel_address2").show();
			bResult = false;
		}
		if($scope.CreateModel.address3 == ""){
			$(".CreateModel_address3").show();
			bResult = false;
		}
		
		
		
		if($scope.CreateModel.price == "" && $scope.CreateModel.price != 0){
			$(".CreateModel_price").show();
			bResult = false;
		}
	 
		if($scope.CreateModel.note == ""){
			$(".CreateModel_note").show();
			bResult = false;
		}
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
     
        var bValid = $scope.validatecheck();
	  
		if(true == bValid){
            //console.log($scope.CriteriaModel);
            fixassetApiService.savefixasset($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });

        }
    }


}]); 